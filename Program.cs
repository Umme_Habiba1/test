﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;

namespace test1

{


    class Program
    {

        static void Main(string[] args)
        {
            int total_quanatity_perproduct = 0;
            float total_revenue_perproduct = 0;
            int popular_product = 0;
            int max = 0;
            float total_revenue = 0;
            int[] compare = new int[100];
            float highest = 0; int month = 0;
            float[] month_revenues = new float[12] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int j = 0;
            try
            {
                using (var reader = new StreamReader(@"C:\Users\Umme Habiba\source\repos\project\test1\data.csv")) //for accessing data of csv file 
                {

                    try
                    {
                        //Open the File
                        StreamWriter sw = new StreamWriter(@"C:\Users\Umme Habiba\source\repos\project\test1\result.txt"); // for writting output in result text file
                        var product1 = new List<ProductSale>(); // create list for storing data of csv file
                        int i = 0;
                        Console.WriteLine("ProductId ----" + "Quantity----" + "unitprice----" + "Saletime");
                        while (!reader.EndOfStream) // loop for reading  all data of  csv file
                        {
                            int id, quantity;
                            float price;
                            DateTime datetime;
                            var line = reader.ReadLine(); // reading line of csv file
                            var d = line.Split(','); // separate every column by splitting by comma
                            if (i > 0) // check condition not to be first row 
                            {
                                id = int.Parse(d[0]); // convert product_id from string to int data type
                                quantity = int.Parse(d[1]); //convert quantity from string to int data type
                                price = float.Parse(d[2]); //convert uunit-price from string to float data type
                                string value2 = d[3].Remove(0, 1); //saletime in string with inverted commas So, removing inverted commas for converting  sale-time from string to datetime data type 
                                string value3 = value2.Remove(19);
                                datetime = DateTime.Parse(value3); //convert sale-time from string to datetime data type
                                //Add a row data(product_id, quantity, unit_price and sale_time) in list 
                                product1.Add(new ProductSale { ProductId = id, Quantity = quantity, unitprice = price, saleTime = datetime });
                                Console.WriteLine(product1[i - 1].ProductId + "----" + product1[i - 1].Quantity + "----" + product1[i - 1].unitprice + "----" + product1[i - 1].saleTime);
                            }

                            i++;
                        }
                        foreach (var pre_product in product1) // loop for retrieving all data stored in list
                        {
                            Boolean checker = false;
                            foreach (int l in compare) // loop for checking condition for repetition of product that already use for calculating quantity and revenue 
                            {
                                if (l == pre_product.ProductId) // if compare list is contained product_id, it means that already calculated quantity and revenue 
                                {
                                    checker = false;
                                    break;
                                }
                                else // if compare list is not contained product_id, it means that there is need to calculate quantity and revenue
                                {
                                    checker = true;
                                }
                            }
                            if (checker == true)
                            {
                                compare[j] = pre_product.ProductId; //Add product_id in compare array for ensuring that quantity and revenue of that product_id is already calculated
                                j++;

                                total_quanatity_perproduct = 0;
                                total_revenue_perproduct = 0;
                                // select all rows of same product_id 
                                var perproduct_data = from p1 in product1
                                                      where p1.ProductId == pre_product.ProductId
                                                      select p1;

                                //loop for calculating quantity and revenue of per-product, total revenue of all products and revenue of per-month
                                foreach (var qp in perproduct_data)
                                {

                                    total_quanatity_perproduct += qp.Quantity; //quantity  of per - product
                                    total_revenue_perproduct += qp.Quantity * qp.unitprice; //revenue of per - product
                                    month_revenues[qp.saleTime.Month] += qp.Quantity * qp.unitprice; //revenue of per - month
                                    total_revenue += qp.Quantity * qp.unitprice; //total revenue of all products

                                }
                                if (max < total_quanatity_perproduct) // calculate most popular product based on quantity sold
                                {
                                    max = total_quanatity_perproduct;
                                    popular_product = pre_product.ProductId;
                                }

                                Console.WriteLine(" Product_ID  " + pre_product.ProductId + "  total quantity " + total_quanatity_perproduct + ",  total revenue  " + total_revenue_perproduct);
                                // write quantity and revenue of per-product in result.txt file
                                sw.WriteLine(" Product_ID  " + pre_product.ProductId + "  total quantity " + total_quanatity_perproduct + ",  total revenue  " + total_revenue_perproduct);
                            }

                        }
                        //loop for calculating month with highest revenue
                        for (int s = 0; s < month_revenues.Length; s++)
                        {

                            if (highest < month_revenues[s])
                            {
                                highest = month_revenues[s];
                                month = s;
                            }
                        }
                        Console.WriteLine("The most popular product based on quantity sold is " + popular_product);
                        Console.WriteLine("The total revenue of all product is " + total_revenue);
                        Console.WriteLine("The month with highest revenu " + highest + "  is " + month);
                        //write popular product base on quantity sold in result.txt file
                        sw.WriteLine("The most popular product based on quantity sold is " + popular_product);
                        //write total revenue of all product in result.txt file
                        sw.WriteLine("The total revenue of all product is " + total_revenue);
                        //write month with highest revenu in result.txt file
                        sw.WriteLine("The month with highest revenue " + highest + "  is " + month);
                        //close the file
                        sw.Close();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Exception: " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
        }

    }
}

public class ProductSale //class for representation of data of csv file
{
    public int ProductId { get; set; } // representation of Product_ID column
    public int Quantity { get; set; } // representation of Quantity column
    public float unitprice { get; set; } // representation of Unit_Price column
    public DateTime saleTime { get; set; } // representation of Sale_Time column

}